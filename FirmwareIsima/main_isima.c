#include <stdio.h>
#include <stdlib.h>

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"
#include "timers.h"
#include "event_groups.h"

#include "HardwareEmu.h"
#include "ControlPanelClient.h"

/** INFORMATIONS SUR LES PERIPHERIQUES ET COMMANDES
---------------------------------------------------
'V' : Vitesse du moteur (0-50)
'D' : Angle de braquage des roues avant en 1/10° de degré (10 -> 1°)
'T' : Vitesse de rotation de la tourelle du télémètre en 1/10°/s
'R' : Lecture de l'Azimuth de la tourelle en 1/10°
'U' : Distance mesure par le télémètre en 1/100 de mètre (en cm)
'X' : Position absolue X en cm
'Y' : Position absolue Y en cm
'Z' : Position absolue Z en cm
'N' : Numéro de la voiture (en fonction de l'ordre de connection)
'E' : Lecture des evènements (cf Annexe 2)
'H' : Donne le temps de course actuel
'S' : Temps du tour précédent
'M' : Mode de course :
    8 bits de poids fort: 1 Attente, 2 course, 3 essais libres)
    8 bits de poids faible : numero de piste
'C' : Informations sur le dernier capteur touché :
    8 bits de poids faible : numéro du capteur
    8 bits de poids fort : couleur ('R','J' ou 'V')
'J' : Proposition d'un code de dévérouillage.
    Une valeur de 0 à 5 par quartet.
'j' : Récupération du résultat de dernier code envoyé.
    0x77 si aucun code n'a été soumis.
    <0 si la réponse n'est pas disponible.
    0x0a0b avec a-> nombre de couleurs bien placées et b -> couleurs présentes mais mal placées.
'I' : Définition du nom du véhicule.
    Doit débuter par le caractère '#' et entraine le chargement de la configuration de piste correspondant au nom du véhicule si le nom se termine par '*'
'K' : Téléportation de la voiture sur le troncon de piste N (correspondant au capteur vert numero N).
    Attention à n'utiliser que pour des tests, les scores sont invalidés !


Contenu du Capteur de couleur;
Bit 0 : Point de passage Vert, remis à zéro lors de la lecture du périphérique 'C'
    1 : Point de passage Jaune, remis à zéro lors de la lecture du périphérique 'C'
    2 : Point de passage Rouge, remis à zéro lors de la lecture du périphérique 'C'
    3 : Point de passage Bleu, remis à zéro lors de la lecture du périphérique 'C'
    4 : Point de passage Cyan, remis à zéro lors de la lecture du périphérique 'C'
    5 : non utilisés
    6 : Collision avec le sol, Remise à zéro au changement de piste.
    7 : Point de passage course (vert), remis à zéro lors de la lecture du périphérique 'C'
    8 : La piste a changé , remis à zéro lors de la lecture du périphérique 'M'
    9 : Le mode de course a changé , remis à zéro lors de la lecture du périphérique 'M'
   10 : non utilisé
   11 : Le dernier point de passage est atteint la course est finie , remis à zéro au changement du mode de course.
   12 : La voiture est sortie de la piste.
   13 : Utilisation de la fonction de téléportation. Classement Invalidé. Remis à zero au changement de piste ou du mode de course.
   14 : Faux départ -> destruction de la voiture , remise à zéro au changement du mode de course.
   15 : Le Feu est Rouge ou orange
**/

QueueHandle_t queueTx,queueRx;

// Couleur de ligne
const unsigned short GREEN = 0x5600;
const unsigned short YELLOW = 0x4A00;
const unsigned short RED = 0x5200;
const unsigned short BLUE;
const unsigned short CYAN = 0x4200;

// Accès au LCD par ligne de l'écran
const unsigned char LCD1 = 0x80;
const unsigned char LCD2 = 0xC0;
const unsigned char LCD3 = 0x90;
const unsigned char LCD4 = 0xD0;

// Paramètres de l'azimuth
int azimuthTourelle = 0;
int consigneAzimuth = 45;
int distTourelle = 0;

// Consigne de position en fonction de la distance
const float startDist = 706.5;
float consigneDist = startDist;
float lastDist = startDist;

// Paramètres de la piste
int feuCouleur = -1;
int piste = 0;
int dernierCapteur = 0;
int fin = 0;
int nbTour = 0;
char couleurCapteur[10];
float tempsActuel = 0;
int speed = 0;

// Dernière touche appuyé par l'utilisateur
int lastkey = 0;

// Booléen pour bloquer des systèmes
int lockTourelle = 0;
int filter = 1;

//--------------------------------------------------------------
// Initialisation des paramètres de la voiture
//--------------------------------------------------------------
void Initialisation(void * queue){
    int i;
    char nom[]="#KANARDO*";
    CanFrame cmd={{'I',0,0}};

	for(i=0;i<9 && nom[i];i++){
		cmd.data.val=nom[i];
        xQueueSend(queueTx,&cmd,portMAX_DELAY);
		vTaskDelay(pdMS_TO_TICKS(10));
	}
    vTaskDelay(pdMS_TO_TICKS(1000));
    vTaskDelete( NULL );
}

void timer1(TimerHandle_t p){
    static int j=0;
    j=(j)?0:1;
    led_j(j);

}

//--------------------------------------------------------------
// Ecritue sur le bus CAN à partir de la queue d'envoie
//--------------------------------------------------------------
void Write_can(void *queue) {
    CanFrame com;
    while(1) {
        if (xQueueReceive(queueTx, &com, portMAX_DELAY) == pdPASS) {
            while(!CanTxReady())
                vTaskDelay(1);
            CanTxSendFrame(com);
        }
        //
    }
}

//--------------------------------------------------------------
// Lecture sur le bus CAN et envoie sur la queue de reçue
//--------------------------------------------------------------
void Read_can(void *queue) {
    CanFrame rep;
    while(1) {
        while (!CanRxFrameAvaible())
            vTaskDelay(1);
        CanRxReadFrame(&rep);
        xQueueSend(queueRx, &rep, portMAX_DELAY);
    }
}


//--------------------------------------------------------------
// Gesition du parcours de chaque piste
//--------------------------------------------------------------
void parcoursPiste(void* queue){
    while(!fin) {
        vTaskDelay(3);
        switch (piste) {
            //--------------------------------------------------------------
            // Pour cette piste, il suffit simplement de réduire la vitesse
            // dans les tournants à 40, 70 sinon.
            //--------------------------------------------------------------
            case 1 :
                filter = 0;
                if ((dernierCapteur & 0xff00) == GREEN) {
                    speed = 70;
                } else {
                    speed = 40;
                }
                break;
            //--------------------------------------------------------------
            // Pour cette piste, c'est comme la verte, à l'exception qu'il
            // faut prendre en compte la montée, car celle-ci peut faire
            // tomber le véhicule si elle prise à 70.
            // (plus un petit trou dans la barrière pris en compte ailleurs)
            //--------------------------------------------------------------
            case 2 :
                if ((dernierCapteur & 0xff00) == GREEN) {
                    speed = 70;
                } else if(dernierCapteur == 0x7201) {
                    speed = 20;
                } else if(dernierCapteur == 0x7202) {
                    speed = 70;
                } else {
                    speed = 40;
                }
                break;
            //--------------------------------------------------------------
            // Pour cette piste, il faut désactiver le contrôle des roues
            // avec le télémètre, car sinon la voiture tourne avant
            // d'effectuer le saut. De plus, il faut prendre en compte la
            // reception du saut avec un filtre permettant d'activer et
            // desactiver la marge d'erreur.
            //--------------------------------------------------------------
            case 3 :
                if ((dernierCapteur & 0xff00) == GREEN) {
                    speed = 70;
                } else if(dernierCapteur == 0x7201) {
                    filter = 0;
                    lockTourelle = 1;
                    speed = 43;
                } else if (dernierCapteur == 0x7204) {
                    lockTourelle = 0;
                    speed = 10;
                } else if (dernierCapteur == 0x7202) {
                    filter = 1;
                    speed = 40;
                } else if(dernierCapteur == 0x7205) {
                    speed = 50;
                } else if(dernierCapteur == 0x7206) {
                    speed = 20;
                }  else {
                    speed = 40;
                }
                break;
            //--------------------------------------------------------------
            // Pour cette piste, il faut esquiver les tonneaux au départ.
            // Pour cela, les deux premiers sont franchies sans télémètre,
            // puis le télémètre est activé en réduisant la consigne,
            // permettant de se rapprocher du mur pour éviter les suivants.
            // Dans le cas du saut, même chose que la piste rouge.
            // Enfin, pour le terrain avec des aspérités, on se rapproche
            // du bord, car l'oscillation du laser dépend de la distance
            // par rapport à l'obstacle
            //--------------------------------------------------------------
            case 4 :
                if(dernierCapteur == 0x5601){
                    speed = 20;
                }
                else if ((dernierCapteur & 0xff00) == GREEN) {
                    speed = 50;
                } else if(dernierCapteur == 0x7201) {
                    lockTourelle = 1;
                    speed = 20;
                } else if(dernierCapteur == 0x7202) {
                    lockTourelle = 0;
                } else if(dernierCapteur == 0x7203) {
                    consigneDist = 500;
                } else if(dernierCapteur == 0x7204) {
                    speed = 20;
                    lockTourelle = 1;
                } else if(dernierCapteur == 0x7205) {
                    speed = 40;
                    lockTourelle = 0;
                } else if(dernierCapteur == 0x7206) {
                    lockTourelle = 1;
                    speed = 41;
                } else if(dernierCapteur == 0x7207) {
                    filter = 0;
                    lockTourelle = 0;
                    speed = 10;
                } else if(dernierCapteur == 0x7208) {
                    filter =1;
                    speed = 10;
                    consigneDist = 500;
                    vTaskDelay(pdMS_TO_TICKS(1500));
                    consigneDist = 250;
                    vTaskDelay(pdMS_TO_TICKS(3000));
                } else if(dernierCapteur == 0x7209) {
                    speed = 15;
                } else if(dernierCapteur == 0x720a) {
                    speed = 10;
                    consigneDist = 500;
                    vTaskDelay(pdMS_TO_TICKS(2000));
                    consigneDist = 706.5;
                    vTaskDelay(pdMS_TO_TICKS(2500));
                } else if(dernierCapteur == 0x720b) {
                    speed = 40;
                } else if(dernierCapteur == 0x4a01) {
                    consigneDist = 706.5;
                }  else {
                    speed = 40;
                }
                break;
        }
    }
}

//--------------------------------------------------------------
// Reçoit les données de la queue correspondante
// et les traites en fonction de leur identifiant
//--------------------------------------------------------------
void Receive_Data(void * queue){
    CanFrame rep ;
    while(1){
        if (xQueueReceive(queueRx, &rep, portMAX_DELAY) == pdPASS){
            switch(rep.data.id){
                case 'R' :
                    azimuthTourelle=(signed short) rep.data.val/10 ;
                    break;

                case 'U' :
                    distTourelle=(signed short) rep.data.val ;
                    break;

                case 'M' :
                    feuCouleur =(signed short) rep.data.val >> 15  ;
                    piste =(signed short) rep.data.val & 0x00FF  ;
                    break;

                case 'C' :
                    dernierCapteur =(signed short) rep.data.val;
                    break;

                case 'H' :
                    tempsActuel =(signed short) rep.data.val;
                    break;
            }

        }
    }
}


//--------------------------------------------------------------
// Permet de récupérer la couleur du dernier capteur traverser
//--------------------------------------------------------------
void SensorColorRetrieve(){
    int temp = dernierCapteur >> 8;
    if (temp==86){
        sprintf(couleurCapteur,"Vert    ");
    } else if (temp==74){
        sprintf(couleurCapteur,"Jaune   ");
    } else if (temp==82){
        sprintf(couleurCapteur,"Rouge   ");
    } else if (temp==67){
        sprintf(couleurCapteur,"Bleu    ");
    } else if (temp==66){
        sprintf(couleurCapteur,"Cyan    ");
    }
}


//--------------------------------------------------------------
// Affichage LCD
//--------------------------------------------------------------
void ShowLCD(void * queue){
    while(1){
        char tmp[32];
        sprintf(tmp,"Circuit : %d",piste);
        lcd_com(LCD1);
        lcd_str(tmp);

        SensorColorRetrieve();
        sprintf(tmp,"Capteur : %s",couleurCapteur);
        lcd_com(LCD2);
        lcd_str(tmp);

        sprintf(tmp,"Tours restants:%d",3-nbTour);
        lcd_com(LCD3);
        lcd_str(tmp);

        sprintf(tmp,"Temps :%.2f   ",tempsActuel/100);
        lcd_com(LCD4);
        lcd_str(tmp);
        vTaskDelay(pdMS_TO_TICKS(500));
    }
}

//--------------------------------------------------------------
// Envoie des messages T pour effectuer la rotation de la tourelle
// Envoie des messages R pour retourner l'angle effecitf de la tourelle portant le télémètre
// Calcul de la consigne pour atteindre l'angle de tourelle voulu
//--------------------------------------------------------------
void Rotation_Tourelle(void *queue) {
    CanFrame comT;
    comT.data.id = 'T';
    comT.data.rtr = 0;
    float error = 0;

    CanFrame comR;
    comR.data.id='R';
    comR.data.rtr=1 ;

    while(1) {
        error = consigneAzimuth - azimuthTourelle;
        comT.data.val = error * 10;
        xQueueSend(queueTx,&comT,portMAX_DELAY);
        xQueueSend(queueTx,&comR,portMAX_DELAY);
        vTaskDelay(pdMS_TO_TICKS(20));
    }
}


//--------------------------------------------------------------
// Envoie des messages D pour effectuer la rotation des roues
// Envoie des messages U pour retourner la distance mesurée par le télémètre
// Calcul de la consigne pour atteindre l'angle des roues directrices voulu
//--------------------------------------------------------------
void Rotation_Roue(void *queue) {
    CanFrame comD;
    comD.data.id = 'D';
    comD.data.rtr = 0;
    comD.data.val = 0;
    float error = 0;

    CanFrame comU;
    comU.data.id = 'U';
    comU.data.rtr = 1;

    xQueueSend(queueTx,&comD,portMAX_DELAY);
    vTaskDelay(pdMS_TO_TICKS(500));
    while(1) {
        if (filter){
            if(abs(distTourelle-lastDist)/lastDist>0.2){//***
                distTourelle = lastDist;
            }
        }
        lastDist = distTourelle;
        if(!lockTourelle){
            error = (consigneDist - distTourelle);
            comD.data.val = -error*1.6;
            xQueueSend(queueTx,&comD,portMAX_DELAY);
            xQueueSend(queueTx,&comU,portMAX_DELAY);
        }
        vTaskDelay(pdMS_TO_TICKS(20));
    }
}


//--------------------------------------------------------------
// Permet de lire les messages autres
// M pour le mode de course (Feu, numéro de la piste)
// C pour les informations sur le dernier capteur
// H pour le temps de course
//--------------------------------------------------------------
void ReadMode(void *queue) {
    CanFrame comM;
    comM.data.id = 'M';
    comM.data.rtr = 1;

    CanFrame comC;
    comC.data.id = 'C';
    comC.data.rtr = 1;

    CanFrame comH;
    comH.data.id = 'H';
    comH.data.rtr = 1;

    while(1) {
        xQueueSend(queueTx,&comM,portMAX_DELAY);
        xQueueSend(queueTx,&comC,portMAX_DELAY);
        xQueueSend(queueTx,&comH,portMAX_DELAY);
        vTaskDelay(pdMS_TO_TICKS(100));//*
    }
}

//--------------------------------------------------------------
// Compte le nombre de tours et arrête la voiture au bout de 3 tours
//--------------------------------------------------------------
void CountLaps(void *queue){
    const signed short GoalNumber = 0x5604;
    const int MaxTour = 3;

    while(!fin){
        if(dernierCapteur==GoalNumber){
            nbTour++;
            if(nbTour>=MaxTour){
                fin = 1;
            }
            while(dernierCapteur==GoalNumber){
                vTaskDelay(pdMS_TO_TICKS(10000));
            }
        }
        vTaskDelay(pdMS_TO_TICKS(100));
    }
}

//--------------------------------------------------------------
// Plus utilisé.
// Permet de changer la vitesse de la voiture en fonction
// de la touche appuyer sur le clavier du panneau de contrôle
//--------------------------------------------------------------
void Velocity(void *queue) {
    CanFrame com;
    com.data.id = 'V';
    com.data.rtr = 0;

    while(1) {
        com.data.val = lastkey * 5;
        xQueueSend(queueTx,&com,portMAX_DELAY);
        if (lastkey>1){
            led_r(1);
        } else {
            led_r(0);
        }
        vTaskDelay(pdMS_TO_TICKS(100));
    }
}

//--------------------------------------------------------------
// Permet la gestion de la vitesse du véhicule
// Envoie du message V avec la vitesse en paramètre
//--------------------------------------------------------------

void Start_Speed(void *queue) {
    CanFrame com;
    com.data.id = 'V';
    com.data.rtr = 0;

    vTaskDelay(pdMS_TO_TICKS(4000));
    while(feuCouleur!=0){
        vTaskDelay(pdMS_TO_TICKS(10));
    }
    speed = 40;
    while(!fin) {
        com.data.val = speed;
        xQueueSend(queueTx,&com,portMAX_DELAY);
        vTaskDelay(pdMS_TO_TICKS(20));
    }
    while(1) {
        com.data.val = 0;
        xQueueSend(queueTx,&com,portMAX_DELAY);
        vTaskDelay(pdMS_TO_TICKS(20));
    }
}

// Interruption pour gérer l'appuie sur le clavier du panneau de contrôle
uint32_t keyboard(){
    lastkey = KeyboardGetLastKey()-48;
    return pdFALSE;
}

// Timer 2 pour la LED verte
void timer2(TimerHandle_t p){
    static int j=0;
    const float delay = 2000.0f;
    const float rapportCyclique = 0.1f;

    switch(j){
        case 1:
            j=0;
            vTaskDelay(pdMS_TO_TICKS(delay*rapportCyclique));
            break;
        case 0:
            j=1;
            vTaskDelay(pdMS_TO_TICKS(delay*(1-rapportCyclique)));
            break;
        default:
            break;
    }
    led_v(j);
}

int main()
{

TimerHandle_t htimer1;
TimerHandle_t htimer2;

    HardwareInit(8,9);
    ControlPanelInit(7);
    //ControlPanelInitAddr("192.168.1.221",7);

    vPortSetInterruptHandler(7, keyboard);

    const int queueSize = 100;
    const int queueItemSize = sizeof(CanFrame);

    queueRx = xQueueCreate(queueSize, queueItemSize);
    queueTx = xQueueCreate(queueSize, queueItemSize);

    printf("Demarrage des taches\n");
    xTaskCreate(Initialisation,"Tache Init",configMINIMAL_STACK_SIZE,NULL,4,NULL);
    xTaskCreate(Rotation_Tourelle,"Tache Rotation Tourelle",configMINIMAL_STACK_SIZE,NULL,4,NULL);
    xTaskCreate(Rotation_Roue,"Tache Rot Wheel",configMINIMAL_STACK_SIZE,NULL,4,NULL);
    xTaskCreate(ReadMode,"Tache Read Mode",configMINIMAL_STACK_SIZE,NULL,4,NULL);
    xTaskCreate(ShowLCD,"Tache",configMINIMAL_STACK_SIZE,NULL,2,NULL);
    xTaskCreate(Receive_Data,"Tache Receive Data",configMINIMAL_STACK_SIZE,NULL,5,NULL);
    xTaskCreate(Start_Speed,"Tache Speed",configMINIMAL_STACK_SIZE,NULL,4,NULL);
    xTaskCreate(parcoursPiste, "Tache parcours piste",configMINIMAL_STACK_SIZE,NULL,4,NULL);
    xTaskCreate(Write_can, "Tache write Can",configMINIMAL_STACK_SIZE,NULL,5,NULL);
    xTaskCreate(Read_can, "Tache read Can",configMINIMAL_STACK_SIZE,NULL,6,NULL);
    xTaskCreate(CountLaps,"Tache Count Laps",configMINIMAL_STACK_SIZE,NULL,3,NULL);
    htimer1=xTimerCreate("Timer 1",250,pdTRUE,NULL,timer1);
    xTimerStart(htimer1,0);
    htimer2=xTimerCreate("Timer 2",250,pdTRUE,NULL,timer2);
    xTimerStart(htimer2,0);
    vTaskStartScheduler();
    HardwareHalt();
    return 0;
}

